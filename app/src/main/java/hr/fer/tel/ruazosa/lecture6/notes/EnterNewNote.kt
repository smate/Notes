package hr.fer.tel.ruazosa.lecture6.notes

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.j256.ormlite.stmt.UpdateBuilder
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

class EnterNewNote : AppCompatActivity() {

    private var noteTitle: EditText? = null
    private var noteDescription: EditText? = null
    private var storeButton: Button? = null
    private var cancelButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_new_note)

        noteTitle = findViewById(R.id.note_title)
        noteDescription = findViewById(R.id.note_description)

        val intent = getIntent()
        if(intent.hasExtra("position")) {
            val position = intent.getIntExtra("position",1)
            val note2 = NotesModel.notesList[position]
            noteTitle?.setText(note2.noteTitle)
            noteDescription?.setText(note2.noteDescription)

        }

        storeButton = findViewById(R.id.store_button)
        storeButton?.setOnClickListener({
            //Checks if the title and description are both entered.

            if(noteTitle?.text.toString().isNotEmpty() && noteDescription?.text.toString().isNotEmpty()) {
                val note = NotesModel.Note(noteTitle = noteTitle?.text.toString(),
                        noteDescription = noteDescription?.text.toString(), noteTime = Date())
                //NotesModel.notesList.add(note)
                val tableDao = DatabaseHelper(this).getDao(NotesModel.Note::class.java)
                /**
                 * This is run only when the activity was called to edit a note.
                 */
                if(intent.hasExtra("position")) {
                    val position = intent.getIntExtra("position",1)
                    val note2 = NotesModel.notesList[position]
                    val updateBuilder = tableDao.updateBuilder()
                    updateBuilder.where().eq("noteTitle",note2.noteTitle)
                    updateBuilder.updateColumnValue("noteTitle",noteTitle?.text.toString())
                    updateBuilder.updateColumnValue("noteDescription",noteDescription?.text.toString())
                    updateBuilder.update()
                    NotesModel.notesList.clear()
                    NotesModel.notesList.addAll(tableDao.queryForAll())
                }
                else{
                    tableDao.create(note)
                    NotesModel.notesList.clear()
                    NotesModel.notesList.addAll(tableDao.queryForAll())
                }

            }
            else{
                //Makes the user know why the note was not saved.
                Toast.makeText(this,"Input cannot be blank!\n Both fields must be entered.",Toast.LENGTH_LONG).show()
            }
            finish()

        })
        cancelButton = findViewById(R.id.cancel_button)

        cancelButton?.setOnClickListener({
            finish()
        })
    }
}